﻿using System;

namespace Ejercicio2
{
    class Program
    {
        static void Main(string[] args)
        {
            int x;
            x = 1;

            do
            {
                Console.WriteLine($"{x}");
                x++;
            } while (x <= 10);
        }
    }
}
