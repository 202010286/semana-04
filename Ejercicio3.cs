﻿using System;

namespace Ejercicio3
{
    class Program
    {
        static void Main(string[] args)
        {
            int x;
            x = 100;
            while (x <= 120)
            {
                Console.WriteLine($"{x}");
                x += 2;
            }
        }
    }
}
