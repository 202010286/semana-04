﻿using System;

namespace Ejercicio1
{
    class Program1
    {
        static void Main(string[] args)
        {
            int x, cantidad_de_numeros, valor, suma;
            suma = 0;
            x = 1;
            valor = 1;
            Console.WriteLine("Ingrese la cantidad de números que quiere sumar: ");
            Console.SetCursorPosition(49, 0);
            cantidad_de_numeros = int.Parse(Console.ReadLine());

            while (x <= cantidad_de_numeros && cantidad_de_numeros != 0 && valor != 0 && valor > 0)
            {

                Console.WriteLine("\nIngrese un valor: ");
                valor = int.Parse(Console.ReadLine());
                suma = suma + valor;
                x++;
            }
            if (cantidad_de_numeros == 0)
            {
                Console.WriteLine("Debe ingresar una cantidad de números distinta a 0.");
            }
            else if (valor == 0)
            {
                Console.WriteLine("Ingrese un valor distinto un valor 0");
            }
            else if (cantidad_de_numeros < 0)
            {
                Console.WriteLine("Debe ingresar una cantidad de números que no sea negativa.");
            }
            else if (valor < 0)
            {
                Console.WriteLine("No se pueden ingresar valores negativos");
            }
            else
            {
                Console.WriteLine($"\nLa suma de los valores ingresados es: {suma}");
            }
        }
    }

}
